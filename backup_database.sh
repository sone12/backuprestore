#!/bin/bash

# Tar backup av en angitt database
db="";
host="mysql.domeneshop.no";
pass="";

# Oppretter tid/datostreng som blir brukt i filnavn
now=$(date +"%d_%m_%Y_%H_%M_%S");

# Oppretter mappestruktur
mkdir -p $HOME/backups/$(date +"%Y/%m/%d");

# Sleve dumpen av valgt database
mysqldump -h ${db}.mysql.domeneshop.no -utesting2 -p${pass} testing2 > $HOME/backups/$(date +"%Y/%m/%d")/${db}_${now}.sql;