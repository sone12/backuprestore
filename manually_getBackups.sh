#!/bin/bash
clear;
# Spør etter brukernavn
  echo Brukernavn på Domeneshop:;
  read user;

# Spør hvor på maskinen man ønsker å lagre backupen, om ingen er valgt, bruke Desktop
  echo Hvor vil du lagre filene?;
    echo [1] Dropbox;
    echo [2] Jottacloud;
    echo [3] Documents;

    read option

# Sjekker hva som er valgt
    if [ "$option" = "1" ]; then
               echo Du valgte Dropbox;
               chosen="Dropbox";

               elif [ "$option" = "2" ]; then
               echo Du valgte Jottacloud;
               chosen="Jottacloud";

               elif [ "$option" = "3" ]; then
               echo Du valgte Documents;
               chosen="Documents";

               else chosen="Desktop";
            fi

# Setter standardmappe for backups utifra $HOME
  localRoot="$chosen";

# Spør etter destinasjon/undermappe for hvor filene skal lagres
  echo "Hvor i $localRoot vil du lagre filene?";
  read localdir;

# Hvilken mappe på remote server skal lastes ned.
  echo Hvilken mappe vil du hente ned?
  read remotedir;

# Oppretter mappe om den ikke finnes. -p lager hele stien om nødvendig
  mkdir -p "$HOME/$localRoot/$localdir";

# Oppretter variabel med sti til destinasjon
  path="$HOME/$localRoot/$localdir";

# Logger inn med ssh, og henter filene i mappen backups, lagrer den til valgt destinasjon
  scp -r "$user"@login.domeneshop.no:"$remotedir" "$path";

# Skriver ut en melding om at alt er gjenomført og lister opp innholdet
  echo Backup gjenomført. Her er innholdet i "$path";
  ls -lR -a "$path";

# Spør om dette skal bli en rutine jobb
  echo Ønsker du å gjenta denne prosedyren via CRON job?;
  echo [Y][N];
  read CRON;
  
   if [ "$CRON" = "Y" ]; then
     echo Hvor mange minutter mellom hver jobb?
     read MINUTES;

     echo "*/$MINUTES * * * * scp -r $user@login.domeneshop.no:$remotedir $path" | crontab -;
     crontab -l;

  else echo Goodbye...;
  fi

read exit;