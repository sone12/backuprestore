#!/bin/bash

# Velg lokasjon for arkivfil
directory="Desktop/Mine-Backups/backups";

# Filnavn på arkivfil
filename="backup";

# Hvilke mappe skal det tas backup av
backupdirectory="www";

# Oppretter tid/datostreng som blir brukt i filnavn
now=$(date +"%d_%m_%Y_%H_%M_%S");

# Oppretter mappestruktur
mkdir -p $HOME/${directory}/$(date +"%Y/%m/%d");

# Pakker backup
tar -zcvf $HOME/${directory}/$(date +"%Y/%m/%d")/${filename}_$(date +"%d_%m_%Y").tar.gz $HOME/${backupdirectory}/*;
echo Backup utført;